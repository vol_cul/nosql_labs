from selenium.webdriver.support.ui import WebDriverWait
from locators import MainLocators


class BasePageElement(object):

	def __get__(self, obj, owner):
		driver = obj.driver
		WebDriverWait(driver, 10).until(
			lambda driver: driver.find_element(*self.locator))
		element = driver.find_element(*self.locator)
		return element


class AboutCompanyButton(BasePageElement):
	locator = MainLocators.ABOUT_COMPANY_BUTTON