import time

from selenium.webdriver import Chrome
from selenium.webdriver import ActionChains
from basepage import BasePage
from locators import MainLocators
from pagelements import (AboutCompanyButton,
	)
class MainPage(BasePage):

	about_company_button = AboutCompanyButton()

	def __init__(self, driver, url):
		self.driver = driver
		self.url = url
		self.driver.get(self.url)

	def go_to_element(self, element):
		self.driver.maximize_window()
		action = ActionChains(self.driver)
		action.move_to_element(element)
		action.perform()

	def go_to_window(self):
		self.go_to_element(self.about_company_button)

if __name__ == '__main__':
	page = MainPage(Chrome(), 'http://185.65.137.46/')
	page.go_to_window()